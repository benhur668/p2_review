FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY . /code/
RUN pip install -r requirements.txt
RUN python3 manage.py migrate
RUN python manage.py loaddata fixture.json
EXPOSE 8000 
ENTRYPOINT python manage.py runserver 0.0.0.0:8000 

